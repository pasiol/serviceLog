package serviceLog

import (
	"errors"
	"fmt"
	"net"
	"os"

	"github.com/google/uuid"
)

type Service struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"service"`
	Build       string    `json:"build_date,omitempty"`
	Version     string    `json:"version"`
	IPAddr      string    `json:"ip_addr,omitempty"`
	Host        string    `json:"host,omitempty"`
	Environment string    `json:"environment,omitempty"`
	Debugging   bool      `json:"debugging,omitempty"`
}

type Event struct {
	ShortMessage string `json:"short_message"`
	EventDetails string `json:"event_details,omitempty"`
	Succesful    bool   `json:"succesful,omitempty"`
	Severity     string `json:"severity,omitempty"`
	Measurement  string `json:"measurement,omitempty"`
}

func getOutboundIP() (net.IP, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP, nil
}

func SetService(s string, v string, b string, e string, d bool) (Service, error) {
	var service = Service{}
	if len(s) > 0 && len(v) > 0 {
		service.Name = s
		service.Version = v
		service.Environment = e
		service.Debugging = d
	} else {
		return Service{}, errors.New("service name or version can not be null")
	}
	ip, _ := getOutboundIP()

	service.ID = uuid.New()
	service.IPAddr = ip.String()
	service.Host, _ = os.Hostname()
	service.Build = b
	return service, nil

}

func GetLogMessage(s Service, e Event) (string, error) {
	if s.Name == "" || s.Version == "" {
		return "", errors.New("service name or version can not be null")
	}
	row := fmt.Sprintf(";%s;%s;%s;%s;%s;%s;%s;%v;%s;%s;%s;%s;%v", s.Version, s.IPAddr, e.ShortMessage, e.EventDetails, e.Measurement, s.ID.String(), s.Name, e.Succesful, e.Severity, s.Host, s.Build, s.Environment, s.Debugging)
	return row, nil

}
